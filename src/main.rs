use std::io;

fn main() {
    println!("What is the name of the first person playing?");
    let mut player = [String::new(), String::new()];
        
    io::stdin().read_line(&mut player[0])
        .ok()
        .expect("Failure to read line");

    println!("What is the name of the second person playing?");
    io::stdin().read_line(&mut player[1])
        .ok()
        .expect("Failure to read line");
   
    //Sets bindings for heap sizes and converts player strings to immutable
    let mut heap = [3,5,7];
    let mut is_player1 = true;
    let player = [player[0].trim(), player[1].trim()];

    print_board(heap);
    println!("It is {}'s turn", player[0]);
    
    loop{   
   
        let player_move = get_move();
        
        if player_move.1 == 0 {
            println!("\nYou either entered 0 or not an actual number.");
            println!("Please try again.\n");
            continue;
        }

        match player_move.0.trim() {
            
            "a" => {
                        if player_move.1 <= heap[0] {
                            heap[0] -= player_move.1;
                        } else{
                            println!("\nPlease choose a smaller number");
                            continue;
                        }
                    }
            
            "b" => {
                        if player_move.1 <= heap[1] {
                            heap[1] -= player_move.1;
                        } else{
                            println!("\nPlease choose a smaller number");
                            continue;
                        }
                    }
            
            "c" => {
                        if player_move.1 <= heap[2] {
                            heap[2] -= player_move.1;
                        } else{
                            println!("\nPlease choose a smaller number");
                            continue;
                        }
                    }

             _  => {
                 println!("\nPlease make a valid choice: a, b, or c");
                 continue;
             }
        };
    
        if heap[0] == 0 && heap[1] == 0 && heap[2] == 0{
            if is_player1 {
                println!("{} wins!", player[0]);
            } else {
                println!("{} wins!", player[1]);
            }
            break;
        }
       
        print_board(heap);
        switch(&mut is_player1, player);

    }
}

fn print_board(heap: [u32;3]) {
    println!("\nHeap A: {}", heap[0]);
    println!(  "Heap B: {}", heap[1]);
    println!("Heap C: {}\n", heap[2]);
}

fn switch(is_player1: &mut bool, player: [&str;2]){
    if *is_player1 {
        *is_player1 = false;
        println!("It is {}'s turn.", player[1]);
    } else {
        *is_player1 = true;
        println!("It is {}'s turn.", player[0]);
    }
}

fn get_move() -> (String,u32) {
    println!("\nFrom which heap do you want to remove objects?");
   
    let mut pile = String::new();
    
    io::stdin().read_line(&mut pile)
        .ok()
        .expect("Failed to read line");
    
    println!("\nHow many do you want to remove?"); 
    
    let mut to_remove = String::new();
    
    io::stdin().read_line(&mut to_remove)
        .ok()
        .expect("Failed to read line");

    let to_remove: u32 = match to_remove.trim().parse() {
        Ok(num) => num,
        Err(_) => 0,
    };
   
    (pile,to_remove)
}
