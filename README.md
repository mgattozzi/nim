#Nim
The game Nim implemented with Rust.

To play do the following:

git clone https://github.com/mgattozzi/nim.git

cd nim

cargo run

You will need rustc and cargo installed in order for this to work.

####Why?
This is a game that has been made many a time throughout the ages
and I need a small project to begin working on my skills with
the rust language. Plus it's a fun little time waster in and of it's
own right.